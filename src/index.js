import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';

import './index.css';
import Root from './components/Root';
import registerServiceWorker from './registerServiceWorker';
import patientViewerApp from './reducers';

let store = createStore(patientViewerApp);

ReactDOM.render(
  <Root store={store} />,
  document.getElementById('root')
);

registerServiceWorker();

import { connect } from 'react-redux'
import PatientTable from '../components/PatientTable'

const mapStateToProps = (state) => ({
  patients: state.patients
})

const PatientsList = connect(
  mapStateToProps
)(PatientTable)

export default PatientsList
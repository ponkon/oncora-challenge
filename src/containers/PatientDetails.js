import { connect } from 'react-redux'
import Patient from '../components/Patient'

const mapStateToProps = (state, ownProps) => (
  {
    patient: state.patients[ownProps.match.params.patientMRN],
    patientsStats: state.patientsStats
  }
)

const PatientDetails = connect(
  mapStateToProps
)(Patient)

export default PatientDetails
import { combineReducers } from 'redux';
import _ from 'lodash';
import math from 'mathjs';

import Patient from './models/Patient';
import rawPatientsData from './constants/patients';
// import { 
//   SET_ACTIVE_PATIENT
// } from './actions';

// Hardcode patient mock data for now.
const patientsData = _.map(rawPatientsData, (rawPatientData) => new Patient(rawPatientData));
const initialPatientsData = _.keyBy(patientsData, 'mrn');
const weights = _.map(patientsData, (patient) => patient.weight)

const initialPatientsStats = {
  weight: {
    min: math.min(weights),
    median: math.median(weights),
    max: math.max(weights)
  }
}

// No real need for the reducer as of yet because
// the app currently only has a constant set of
// patients.
function patients(state = initialPatientsData, action) {
  switch (action.type) {
    default:
      return state;
  }
}

function patientsStats(state = initialPatientsStats, action) {
  switch (action.type) {
    default:
      return state;
  }
}

const patientViewerApp = combineReducers({
  // activePatient,
  patients,
  patientsStats
})

export default patientViewerApp
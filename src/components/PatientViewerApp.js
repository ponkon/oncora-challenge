import React from 'react';

import PatientList from '../containers/PatientList';

const PatientViewerApp = () => (
  <div style={styles.container}>
    <label style={styles.headline}>Basic Patient Viewer</label>
    <PatientList />
  </div>
)

const styles = {
  container: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center"
  },
  headline: {
    textAlign: "center",
    fontSize: "30px",
    padding: "10px 0px 20px 0px",
    width: "100%"
  }
}

export default PatientViewerApp;

import React from 'react';
import { Link } from 'react-router-dom';

import LineChart1D from './LineChart1D';
  
const Patient = ({ patient, patientsStats }) => (
  <div style={{width: "100%", textAlign: "center"}}>
    <div style={styles.container}>
      <div style={styles.headline}>{patient.getFullName()}</div>
      <div style={styles.contentLine}>
        <div>{patient.mrn}</div>
        <div>{patient.dob.format('M/D/YYYY')}</div>
        <div>{patient.getDemographics()}</div>
      </div>
      <div style={styles.contentLine}>
        <div>{patient.getTumorDetails()}</div>
      </div>
      <div style={styles.contentLine}>
        <label>{`${patient.weight} lb`}</label>
        <LineChart1D 
          sample={patient.weight}
          min={patientsStats.weight.min}
          median={patientsStats.weight.median}
          max={patientsStats.weight.max}
        />
      </div>
      <Link style={styles.backButton} to="/">{"< PATIENT LIST"}</Link>
    </div>
  </div>
)

const styles = {
  backButton: {
    fontSize: "16px",
    backgroundColor: "#33CCFF",
    color: "#FFF",
    margin: "30px 0px 0px 0px",
    padding: "10px",
    borderRadius: "5px"
  },
  container: {
    width: "600px",
    display: "flex",
    flexWrap: "wrap",
    margin: "auto"
  },
  headline: {
    width: "100%",
    fontSize: "30px",
    textAlign: "center",
    padding: "10px 0px"
  },
  contentLine: {
    padding: "20px",
    width: "100%",
    display: "flex",
    justifyContent: "space-between"
  }
}

export default Patient

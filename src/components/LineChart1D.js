import React from 'react';

const chartWidth = 2000;
const chartHeight = 100;
const strokeWidth = 10;
const strokeColor = "#4f4f4f"


const LineChart1D = ({ sample, min, median, max }) => {
  const medianX = ((median - min) / (max - min))*chartWidth;
  const sampleX = ((sample - min) / (max - min))*chartWidth;

  return <svg height="20" viewBox={`${-chartHeight / 2} 0 ${chartWidth + chartHeight} ${chartHeight}`} preserveAspectRatio="xMinYMin meet">
    <line x1="0" y1="0" x2="0" y2={chartHeight} stroke-width={strokeWidth} stroke={strokeColor}/>
    <line x1={medianX} y1="0" x2={medianX} y2={chartHeight} stroke-width={strokeWidth} stroke={strokeColor}/>
    <line x1={chartWidth} y1="0" x2={chartWidth} y2={chartHeight} stroke-width={strokeWidth} stroke={strokeColor}/>
    <line x1="0" y1={chartHeight / 2} y2={chartHeight / 2} x2={chartWidth} y2={chartHeight / 2} stroke-width={strokeWidth} stroke={strokeColor}/>
    <circle cx={sampleX} cy={chartHeight / 2} r={chartHeight / 2 - strokeWidth /2} stroke="#4f4f4f" stroke-width={strokeWidth} fill="#6bf178"/>
  </svg>
}

export default LineChart1D

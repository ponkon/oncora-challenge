import React from 'react';
import _ from 'lodash';

import PatientTableItem from './PatientTableItem';

const PatientTable = ({ patients }) => (
  <div style={styles.container}>
    <table style={styles.table}>
      <thead style={styles.tableHeader}>
        <tr>
          <th>Name</th>
          <th>MRN</th>
          <th>DOB</th>
          <th>Demographics</th>
          <th>Treatment Site</th>
        </tr>
      </thead>
      <tbody>
        {_.map(patients, (patient) => {
          return <PatientTableItem key={patient.mrn} patient={patient} />
        })}
      </tbody>
    </table>
  </div>
)

const styles = {
  container: {
    width: '800px',
    margin: 'auto'
  },
  table: {
    width: '100%'
  },
  tableHeader: {
    textAlign: 'left',
    fontSize: '20px'
  }
}

export default PatientTable
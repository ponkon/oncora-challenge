import React from 'react';
import { Link } from 'react-router-dom';

const LinkableCell = ({ text, pathname }) => (
  <td>
    <Link to={{pathname: `/${pathname}`}}>
      {text}
    </Link>
  </td>
)

const PatientTableItem = ({ patient }) => (
  <tr style={styles.row}>
    <LinkableCell text={patient.getFullName()} pathname={patient.mrn}/>
    <LinkableCell text={patient.mrn} pathname={patient.mrn}/>
    <LinkableCell text={patient.dob.format('M/D/YYYY')} pathname={patient.mrn}/>
    <LinkableCell text={patient.getDemographics()} pathname={patient.mrn}/>
    <LinkableCell text={patient.treatmentSite} pathname={patient.mrn}/>
  </tr>
)

const styles = {
  row: {
    fontSize: '20px'
  }
}

export default PatientTableItem

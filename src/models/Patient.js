import moment from 'moment';

export default class Patient {
  constructor(attr) {
    this.mrn = attr.mrn;
    this.name = attr.name;
    this.sex = attr.sex;

    this.dob = (attr.dob) ? moment(attr.dob, 'MM/DD/YYYY') : null;
    this.age = moment().diff(this.dob, 'years');
    this.treatmentSite = attr.treatment_site;
    this.weight = attr.weight;
    this.tumorSize = attr.tumor_size_cm;
    this.histology = attr.histology
  }

  getDemographics() {
    return `${this.age} y.o. ${this.sex}`
  }

  getFullName() {
    return `${this.name.last}, ${this.name.first}`
  }

  getTumorDetails() {
    return `${this.tumorSize} cm, ${this.histology}, ${this.treatmentSite}`
  }
}